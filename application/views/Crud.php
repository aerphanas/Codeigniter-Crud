<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>halaman crud</title>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<table class="table">
						<tr>
							<th>no</th>
							<th>name</th>
							<th>username</th>
							<th>email</th>
							<th>action</th>
						</tr>
						<?php $no =0 ?>
						<?php foreach ($hasil->result() as $value): ?>
						<tr>
							<td><?php echo ++$no; ?></td>
							<td><?php echo $value->name; ?></td>
							<td><?php echo $value->username; ?></td>
							<td><?php echo $value->email; ?></td>
							<td>
								<a href="<?php echo base_url('crud/delete/').$value->id; ?>">delete</a>
								<a href="<?php echo base_url('/crud/update/').$value->id; ?>">edit</a>
							</td>
						</tr>
						<?php endforeach ?>
					</table>
					<p><a class="btn btn-outline-info" href="<?php echo base_url('crud/add/'); ?>">add</a></p>
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>
	</body>
</html>