<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title></title>
		<link rel="stylesheet" href="<?php echo base_url('asset/bootstrap-4.0.0-dist/css/bootstrap.css'); ?>">
		<link rel="stylesheet" href="<?php echo base_url('asset/font-awesome-4.7.0/css/font-awesome.css'); ?>">
	</head>
	<body class="alert alert-warning">
		<div class="container">
			<div class="row">
				<div class="col-sm-3"></div>
				<div class="col-sm-6">
					<div class="alert alert-danger" role="alert" style="margin-top: 20%">
						<p align="center"><i class="fa fa-warning" style="font-size: 220px;"></i></p>
						<p align="center" class="display-1">404 Not Found</p>
						<p align="center">"Better check yourself, you're not looking too good."</p>
					</div>	
				</div>
				<div class="col-sm-3"></div>
			</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url('asset/bootstrap-4.0.0-dist/popper.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('asset/bootstrap-4.0.0-dist/jquery-3.2.1.slim.min.js'); ?>"></script>
		<script type="text/javascript" src="<?php echo base_url('asset/bootstrap-4.0.0-dist/js/bootstrap.js'); ?>"></script>
	</body>
</html>