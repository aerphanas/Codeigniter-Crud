<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
	}

	// List all your items
	public function index()
	{
		$this->load->view('Headcss');
		$this->load->view('Navbar');
		$data['hasil'] = $this->db->get('user');
		$this->load->view('Crud', $data);
		$this->load->view('Bottomscript');
	}

	// Add a new item
	public function add()
	{
		$this->load->view('Headcss');
		$this->load->view('Navbar');
		$this->load->view('Add');
		$this->load->view('Bottomscript');
	}

	public function action_add()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('pass')
		);		
		$this->db->insert('user', $data);
		redirect(base_url(),'refresh');
	}

	//Update one item
	public function update( $id = NULL )
	{
		$this->load->view('Headcss');
		$this->load->view('Navbar');
		$this->db->where('id', $id);
		$data['hasil'] = $this->db->get('user');
		$this->load->view('Update', $data);
		$this->load->view('Bottomscript');
	}

	public function action_update( $id = '')
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('pass')
		);
		$this->db->where('id', $id);
		$this->db->update('user', $data);
		redirect(base_url(),'refresh');
	}

	//Delete one item
	public function delete( $id = NULL )
	{
		$this->db->where('id', $id);
		$this->db->delete('user');
		redirect(base_url(),'refresh');
	}
}

/* End of file Crud.php */
/* Location: ./application/controllers/Crud.php */
